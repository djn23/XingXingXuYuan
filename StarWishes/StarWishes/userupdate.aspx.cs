﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class userupdate : System.Web.UI.Page
{
    StarService.StarService s = new StarService.StarService();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (DropDownList1.Items.Count == 0)
        {
            for (int i = 1950; i < 2019; i++)
            {
                DropDownList1.Items.Add(i.ToString());
            }
        }
        if (DropDownList2.Items.Count == 0)
        {
            for (int i = 1; i < 13; i++)
            {
                DropDownList2.Items.Add(i.ToString());
            }
        }
        if (DropDownList3.Items.Count == 0)
        {
            for (int i = 1; i < 32; i++)
            {
                DropDownList3.Items.Add(i.ToString());
            }
            int userid = Convert.ToInt32(Session["userid"].ToString());
            DataTable dt = s.UserInfo(userid);
            username.Text = dt.Rows[0]["username"].ToString();
            password.Text = dt.Rows[0]["userpwd"].ToString();
            DropDownList1.SelectedValue = Convert.ToDateTime(dt.Rows[0]["birthdate"].ToString()).Year.ToString();
            DropDownList2.SelectedValue = Convert.ToDateTime(dt.Rows[0]["birthdate"].ToString()).Month.ToString();
            DropDownList3.SelectedValue = Convert.ToDateTime(dt.Rows[0]["birthdate"].ToString()).Day.ToString();
            if (Convert.ToInt32(dt.Rows[0]["usersex"].ToString()) == 1)
            {
                RadioButton1.Checked = true;
            }
            else
            {
                RadioButton2.Checked = true;
            }
            realname.Text = dt.Rows[0]["realname"].ToString();
            usertel.Text = dt.Rows[0]["usertel"].ToString();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        int userid = Convert.ToInt32(Session["userid"].ToString());
        string username = this.username.Text.Trim();
        string pwd = this.password.Text.Trim();
        int sex = 1;
        if(RadioButton2.Checked)
        {
            sex = 2;
        }
        string bir = DropDownList1.SelectedItem.ToString() + "-" + DropDownList2.SelectedItem.ToString() + "-" + DropDownList3.SelectedItem.ToString();
        string realname = this.realname.Text.Trim();
        string tel = usertel.Text.Trim();
        int k= Convert.ToInt32( s.UserUpdate(userid, username, pwd, sex, bir, realname, tel));
        if(k>0)
        {
            Response.Write("<script>alert('用户修改成功！')</script>");
        }
        else
        {
            Response.Write("<script>alert('用户修改失败！')</script>");
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.location.href='WishList.aspx'</script>");
    }
}