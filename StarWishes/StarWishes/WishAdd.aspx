﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WishAdd.aspx.cs" Inherits="WishAdd" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>星星许愿—愿望添加</title>
</head>
<body style="background: url(/css/Register.jpg)">
    <form id="form1" runat="server">
        <div style="font-size: 30px;">
            <div style="font-size: 80px; text-align: center; font-family: 楷体;">
                <asp:Label ForeColor="LightBlue" ID="lbl" runat="server" Text="许愿"></asp:Label>
                <br />
                <br />
            </div>
            <div style="float: left; text-align: center; width: 100%; color: white">
                <asp:Label ID="Label1" runat="server" Text="愿望标题:"></asp:Label>
                <asp:TextBox ID="Wishtitle" runat="server"></asp:TextBox>
                <br />
                <br />
            </div>
            <div style="float: left; text-align: center; width: 88%; color: white">
                <asp:Label ID="Label2" runat="server" Text="愿望内容:"></asp:Label>
            </div>
            <div style="float: left; text-align: center; width: 110%; color: white">
                <asp:TextBox ID="WishContent" runat="server" TextMode="MultiLine" Height="112px" Width="189px"></asp:TextBox>
                <br />
                <br />
            </div>
            <div style="float: left; text-align: center; width: 100%; color: white">
                <asp:Label ID="Label4" runat="server" Text="愿望类型:"></asp:Label>
                <asp:RadioButton ID="RadioButton1" runat="server" Text="免费" GroupName="1" Checked="True" />
                <asp:RadioButton ID="RadioButton2" runat="server" Text="有偿" GroupName="1" />
                <br />
                <br />
            </div>
            <div style="float: left; text-align: center; width: 100%; color: white">
                <asp:Label ID="Label3" runat="server" Text="愿望报酬:"></asp:Label>
                <asp:TextBox ID="Wishreword" runat="server"></asp:TextBox>
                <br />
                <br />
            </div>
            <div style="float: left; text-align: center; width: 95%; color: white">
                <asp:Label ID="Label5" runat="server" Text="团队选择:"></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div style="float: left; text-align: center; width: 100%; color: white">
                <asp:Button ID="btntj" runat="server" Text="提交" OnClick="btntj_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" Text="返回" OnClick="btncancel_Click" />
            </div>
        </div>
    </form>
</body>
</html>
