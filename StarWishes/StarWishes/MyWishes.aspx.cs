﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MyWishes : System.Web.UI.Page
{
    StarService.StarService ss = new StarService.StarService();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblname.Text = Session["username"].ToString();
        DataTable dt = ss.WishInfo(Convert.ToInt32(Session["userid"]));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
}