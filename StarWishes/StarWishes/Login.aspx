﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>用户登录</title>
    <link href="css/index_style.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <!-----HEADER STAR----->

    <div class="header" id="demo">

        <div class="nav">

            <form id="form1" runat="server">
                <a href="Main.aspx" style="text-decoration: none">
                    <div style="font-size: 100px; color: lightblue; text-align: center; font-family: 楷体;">星星许愿</div>
                </a>
                <br />
                <br />
                <div style="font-size: 20px; color: aliceblue; text-align: center; font-family: 宋体; letter-spacing: 20px;"><strong>点击上方标题即可返回首页</strong></div>
                <br />
                <br />
                <div style="padding-top: 35px;">
                    <div style="width: 40%; height: 30px; float: left; text-align: right; padding-right: 15px; color: aliceblue; font-size: 25px; font-family: 宋体;">用户名</div>
                    <%--<div style="width:58%; height:30px;float:left;"><input id="username" name="username" type="text" /></div>--%>
                    <asp:TextBox ID="TextBox1" runat="server" Height="25px"></asp:TextBox>
                </div>
                <br />
                <div style="padding-top: 55px;">
                    <div style="width: 40%; height: 30px; float: left; text-align: right; padding-right: 15px; color: aliceblue; font-size: 25px; font-family: 宋体;">密&nbsp;码</div>
                    <%--<div style="width:58%; height:30px;float:left;"><input id="password" name="password" type="text" /></div>--%>
                    <asp:TextBox ID="TextBox2" runat="server" Height="25px" TextMode="Password"></asp:TextBox>
                </div>
                <br />
                <div style="padding-top: 55px;">
                    <div style="width: 20%; height: 30px; float: left; text-align: right;"></div>
                    <%--<div style="width:25%; height:30px;float:left; text-align:right; padding-left:10px;color:aliceblue;font-size:25px;font-family:宋体;">登录</div>--%>
                    <asp:Button ID="Button1" runat="server" Text="登录" Width="25%" Height="30px" Style="border: 0; background-color: transparent; outline: none; float: left; color: aliceblue; font-size: 25px; font-family: 宋体; padding-left: 80px;" OnClick="Button1_Click" />
                    <a href="Register.aspx" style="text-decoration: none;">
                        <div style="width: 20%; height: 30px; float: left; text-align: left; padding-left: 80px; color: aliceblue; font-size: 25px; font-family: 宋体;">注册</div>
                    </a>
                </div>

            </form>
        </div>
        <%--<div style="color:white">123</div>--%>

        <div class="canvaszz"></div>
        <canvas id="canvas"></canvas>
    </div>


    <!-----HEADER END----->

    <!--用来解决视频右键菜单，用于视频上面的遮罩层 START-->
    <div class="videozz"></div>
    <!--用来解决视频右键菜单，用于视频上面的遮罩层 END-->

    <!--音乐 START-->
    <audio autoplay="autoplay" class="audio">
        <source src="css/Music.mp3" type="audio/mp3" />
        <source src="css/Music.ogg" type="audio/ogg" />
        <source src="css/Music.aac" type="audio/mp4" />
    </audio>
    <!--音乐 END-->


    <script>
        //宇宙特效
        "use strict";
        var canvas = document.getElementById('canvas'),
            ctx = canvas.getContext('2d'),
            w = canvas.width = window.innerWidth,
            h = canvas.height = window.innerHeight,

            hue = 217,
            stars = [],
            count = 0,
            maxStars = 1300;//星星数量

        var canvas2 = document.createElement('canvas'),
            ctx2 = canvas2.getContext('2d');
        canvas2.width = 100;
        canvas2.height = 100;
        var half = canvas2.width / 2,
            gradient2 = ctx2.createRadialGradient(half, half, 0, half, half, half);
        gradient2.addColorStop(0.025, '#CCC');
        gradient2.addColorStop(0.1, 'hsl(' + hue + ', 61%, 33%)');
        gradient2.addColorStop(0.25, 'hsl(' + hue + ', 64%, 6%)');
        gradient2.addColorStop(1, 'transparent');

        ctx2.fillStyle = gradient2;
        ctx2.beginPath();
        ctx2.arc(half, half, half, 0, Math.PI * 2);
        ctx2.fill();

        // End cache

        function random(min, max) {
            if (arguments.length < 2) {
                max = min;
                min = 0;
            }

            if (min > max) {
                var hold = max;
                max = min;
                min = hold;
            }

            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function maxOrbit(x, y) {
            var max = Math.max(x, y),
                diameter = Math.round(Math.sqrt(max * max + max * max));
            return diameter / 2;
            //星星移动范围，值越大范围越小，
        }

        var Star = function () {

            this.orbitRadius = random(maxOrbit(w, h));
            this.radius = random(60, this.orbitRadius) / 8;
            //星星大小
            this.orbitX = w / 2;
            this.orbitY = h / 2;
            this.timePassed = random(0, maxStars);
            this.speed = random(this.orbitRadius) / 200000;
            //星星移动速度
            this.alpha = random(2, 10) / 10;

            count++;
            stars[count] = this;
        }

        Star.prototype.draw = function () {
            var x = Math.sin(this.timePassed) * this.orbitRadius + this.orbitX,
                y = Math.cos(this.timePassed) * this.orbitRadius + this.orbitY,
                twinkle = random(10);

            if (twinkle === 1 && this.alpha > 0) {
                this.alpha -= 0.05;
            } else if (twinkle === 2 && this.alpha < 1) {
                this.alpha += 0.05;
            }

            ctx.globalAlpha = this.alpha;
            ctx.drawImage(canvas2, x - this.radius / 2, y - this.radius / 2, this.radius, this.radius);
            this.timePassed += this.speed;
        }

        for (var i = 0; i < maxStars; i++) {
            new Star();
        }

        function animation() {
            ctx.globalCompositeOperation = 'source-over';
            ctx.globalAlpha = 0.5; //尾巴
            ctx.fillStyle = 'hsla(' + hue + ', 64%, 6%, 2)';
            ctx.fillRect(0, 0, w, h)

            ctx.globalCompositeOperation = 'lighter';
            for (var i = 1, l = stars.length; i < l; i++) {
                stars[i].draw();
            };

            window.requestAnimationFrame(animation);
        }

        animation();
    </script>
</body>
</html>
