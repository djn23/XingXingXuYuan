﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WishList : System.Web.UI.Page
{
    StarService.StarService ss = new StarService.StarService();
    protected void Page_Load(object sender, EventArgs e)
    { 
        Label1.Text = Session["username"].ToString();
        DataTable dt = ss.getWishInfo();
        string  wishid = dt.Rows[0]["wishid"].ToString();
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        Session["wishid"] = wishid;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("WishAdd.aspx");
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("WishInfo.aspx");
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("userupdate.aspx");
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("MyWishes.aspx");
    }
}