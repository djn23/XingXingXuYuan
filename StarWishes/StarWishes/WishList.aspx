﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WishList.aspx.cs" Inherits="WishList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>愿望池</title>
</head>
<body style="font-family: 楷体; background: url(/images/Star2.jpg)">
    <form id="form1" runat="server">
        <div style="text-align: right;width:100%;color:white; font-size:30px;">
                <asp:Button ID="Button1" runat="server" Text="许愿" style="background-color:red;border:0;" OnClick="Button1_Click" />
                &nbsp;
                <asp:Button ID="Button2" runat="server" Text="用户修改" style="background-color:red;border:0;" OnClick="Button2_Click" />
                &nbsp;
                <asp:Button ID="Button3" runat="server" Text="还愿" style="background-color:red;border:0;" OnClick="Button3_Click" />
            &nbsp;
            <asp:Button ID="Button4" runat="server" Text="我的愿望" style="background-color:red;border:0;" OnClick="Button4_Click" />
            </div>
        <br />
        <div style="text-align: center; ">
            <a href="Main.aspx" style="text-decoration: none;color: red;">
            <div style="font-size: 50px;">
                欢迎来到星星许愿网站。<asp:Label ID="Label1" runat="server"></asp:Label>!
            </div>
                </a>
            <div style="font-size: 30px;color:white">点击上方标题，返回首页</div>
            <div style="width: 1350px; height: auto; margin-left: 100px; margin-top: 20px;">
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <a href="WishInfo.aspx">
                            <div style="color: white; width: 250px; height: 180px; border: double; border-color: lightgreen; text-align: center; padding-top: 5px; float: left; margin: 5px;">
                                <br />
                                <strong><%#Eval("wishtitle") %>(<%#Eval("istrue") %>)</strong><h1 style="visibility:hidden"><%#Eval("wishid") %></h1>
                                <strong><%#Eval("userid") %></strong>
                                <h5><%#Eval("wishcontent") %></h5>
                            </div>
                        </a>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
          
    </form>
      
</body>
</html>
