﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WishInfo : System.Web.UI.Page
{
    StarService.StarService s = new StarService.StarService();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt = s.WishInfo(Convert.ToInt32(Session["userid"]));
            Session["wishid"] = dt.Rows[0]["wishid"].ToString();
            Wtitle.Text = dt.Rows[0]["wishtitle"].ToString();
            WContent.Text = dt.Rows[0]["wishcontent"].ToString();
            int type = Convert.ToInt32(dt.Rows[0]["wishtype"].ToString());
            if (type == 1)
            {
                RadioButton1.Checked = true;
            }
            else
            {
                RadioButton2.Checked = true;
            }
            if (Convert.ToInt32(dt.Rows[0]["teamid"].ToString()) == 0)
            {
                DropDownList1.SelectedItem.Text = "金牌团队";
            }
            else if (Convert.ToInt32(dt.Rows[0]["teamid"].ToString()) == 1)
            {
                DropDownList1.SelectedItem.Text = "银牌团队";
            }
            else
            {
                DropDownList1.SelectedItem.Text = "铜牌团队";
            }
        }
    }

    protected void btnTJ_Click(object sender, EventArgs e)
    {
        string isTrue = "已完成";
        int wishid = Convert.ToInt32(Session["wishid"]);
        int userid = Convert.ToInt32(Session["userid"]);
        string  k = s.getWishFinish(isTrue, userid, wishid);
        if(k=="1")
        {
            Response.Write("<script>alert('还愿成功！即将跳转到愿望池！！！');window.location.href='WishList.aspx';</script>");
        }
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {

    }
}