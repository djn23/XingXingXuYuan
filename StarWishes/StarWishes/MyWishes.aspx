﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyWishes.aspx.cs" Inherits="MyWishes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body style="background:url(/images/Star2.jpg)">
    <form id="form1" runat="server">
        
        <div style="text-align:center">
            <a href="WishList.aspx" style="text-decoration:none;color:red">
            <div style="font-size: 50px;">
                欢迎查看您的愿望。<asp:Label ID="lblname" runat="server"></asp:Label>!
            </div></a>
            <br />
            <div style="font-size: 20px; color: aliceblue; text-align: center; font-family: 宋体; letter-spacing: 20px;"><strong>点击上方标题即可返回首页</strong></div>
            <div style="width: 1350px; height: auto; margin-left: 100px; margin-top: 20px;">
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>          
                            <div style="color: white; width: 250px; height: 180px; border: double; border-color: lightgreen; text-align: center; padding-top: 5px; float: left; margin: 5px;">
                                <br />
                                <strong><%#Eval("wishtitle") %>(<%#Eval("istrue") %>)</strong><h1 style="visibility:hidden"><%#Eval("wishid") %></h1>
                                <h5><%#Eval("wishcontent") %></h5>
                            </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </form>
</body>
</html>
