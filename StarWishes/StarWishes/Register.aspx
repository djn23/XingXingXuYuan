﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>用户注册</title>
    <link href="css/index_style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <!-----HEADER STAR----->


    <div class="header" id="demo">

        <div class="nav">
            <div style="color: aliceblue; font-size: 20px;">
                <form id="form1" runat="server">
                    <div style="font-size: 80px; text-align: center; font-family: 楷体;">
                        <a href="Main.aspx" style="text-decoration: none;">
                            <asp:Label ForeColor="LightBlue" ID="lbl" runat="server" Text="星星许愿"></asp:Label></a>
                    </div>
                    <div style="font-size: 20px; color: aliceblue; text-align: center; font-family: 宋体; letter-spacing: 20px;"><strong>点击上方标题即可返回首页</strong></div>
                    <br />
                    <div style="float: left; text-align: center; width: 100%">
                        <asp:Label ID="Label1" runat="server" Text="用户名称:"></asp:Label>
                        <asp:TextBox ID="tbUserName" runat="server"></asp:TextBox>
                        <br />
                        <br />
                        <asp:Label ID="Label2" runat="server" Text="密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码:"></asp:Label>
                        <asp:TextBox ID="tbPassword" runat="server" TextMode="Password"></asp:TextBox>
                        <br />
                        <br />
                        <asp:Label ID="Label3" runat="server" Text="确认密码:"></asp:Label>
                        <asp:TextBox ID="tbRassword" runat="server" TextMode="Password"></asp:TextBox>
                        <br />
                        <br />
                    </div>
                    <div style="float: left; text-align: center; width: 100%; letter-spacing: 10px;">
                        <asp:Label ID="Label4" runat="server" Text="性&nbsp;&nbsp;别:"></asp:Label>
                        <asp:RadioButton ID="RadioButton1" runat="server" Text="男" GroupName="1" Checked="True" />
                        <asp:RadioButton ID="RadioButton2" runat="server" Text="女" GroupName="1" />
                        <br />
                        <br />
                    </div>
                    <div style="float: left; text-align: center; width: 100%">
                        <asp:Label ID="Label6" runat="server" Text="真实姓名:"></asp:Label>
                        <asp:TextBox ID="tbRealName" runat="server"></asp:TextBox>
                        <br />
                        <br />
                        <asp:Label ID="Label5" runat="server" Text="出生日期:"></asp:Label>
                        <%--<asp:TextBox ID="tbAge" runat="server"></asp:TextBox>--%>
                        <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>年
                <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"></asp:DropDownList>月
                <asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList>日
            <br />
                        <br />
                        <asp:Label ID="Label7" runat="server" Text="联系电话:"></asp:Label>
                        <asp:TextBox ID="tbTel" runat="server"></asp:TextBox>
                        <br />
                        <br />
                    </div>
                    <div style="float: left; text-align: center; width: 100%; margin-top: 20px;">
                        <asp:Button ID="btnReg" runat="server" Text="注册" Width="25%" Height="30px" Style="border: 0; background-color: transparent; outline: none; float: left; color: aliceblue; font-size: 25px; font-family: 宋体; margin-left: 200px;" OnClick="btnReg_Click" />
                        <asp:Button ID="btnClear" runat="server" Text="清空" Width="25%" Height="30px" Style="border: 0; background-color: transparent; outline: none; float: left; color: aliceblue; font-size: 25px; font-family: 宋体;" OnClick="btnClear_Click" />
                    </div>
                </form>
            </div>
        </div>
        <div class="canvaszz"></div>
        <canvas id="canvas"></canvas>
    </div>


    <!-----HEADER END----->

    <!--用来解决视频右键菜单，用于视频上面的遮罩层 START-->
    <div class="videozz"></div>
    <!--用来解决视频右键菜单，用于视频上面的遮罩层 END-->

    <!--音乐 START-->
    <audio autoplay class="audio">
        <source src="css/Music.mp3" type="audio/mp3">
        <source src="css/Music.ogg" type="audio/ogg">
        <source src="css/Music.aac" type="audio/mp4">
    </audio>
    <!--音乐 END-->


    <script>
        //宇宙特效
        "use strict";
        var canvas = document.getElementById('canvas'),
            ctx = canvas.getContext('2d'),
            w = canvas.width = window.innerWidth,
            h = canvas.height = window.innerHeight,

            hue = 217,
            stars = [],
            count = 0,
            maxStars = 1300;//星星数量

        var canvas2 = document.createElement('canvas'),
            ctx2 = canvas2.getContext('2d');
        canvas2.width = 100;
        canvas2.height = 100;
        var half = canvas2.width / 2,
            gradient2 = ctx2.createRadialGradient(half, half, 0, half, half, half);
        gradient2.addColorStop(0.025, '#CCC');
        gradient2.addColorStop(0.1, 'hsl(' + hue + ', 61%, 33%)');
        gradient2.addColorStop(0.25, 'hsl(' + hue + ', 64%, 6%)');
        gradient2.addColorStop(1, 'transparent');

        ctx2.fillStyle = gradient2;
        ctx2.beginPath();
        ctx2.arc(half, half, half, 0, Math.PI * 2);
        ctx2.fill();

        // End cache

        function random(min, max) {
            if (arguments.length < 2) {
                max = min;
                min = 0;
            }

            if (min > max) {
                var hold = max;
                max = min;
                min = hold;
            }

            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function maxOrbit(x, y) {
            var max = Math.max(x, y),
                diameter = Math.round(Math.sqrt(max * max + max * max));
            return diameter / 2;
            //星星移动范围，值越大范围越小，
        }

        var Star = function () {

            this.orbitRadius = random(maxOrbit(w, h));
            this.radius = random(60, this.orbitRadius) / 8;
            //星星大小
            this.orbitX = w / 2;
            this.orbitY = h / 2;
            this.timePassed = random(0, maxStars);
            this.speed = random(this.orbitRadius) / 100000;
            //星星移动速度
            this.alpha = random(2, 10) / 10;

            count++;
            stars[count] = this;
        }

        Star.prototype.draw = function () {
            var x = Math.sin(this.timePassed) * this.orbitRadius + this.orbitX,
                y = Math.cos(this.timePassed) * this.orbitRadius + this.orbitY,
                twinkle = random(10);

            if (twinkle === 1 && this.alpha > 0) {
                this.alpha -= 0.05;
            } else if (twinkle === 2 && this.alpha < 1) {
                this.alpha += 0.05;
            }

            ctx.globalAlpha = this.alpha;
            ctx.drawImage(canvas2, x - this.radius / 2, y - this.radius / 2, this.radius, this.radius);
            this.timePassed += this.speed;
        }

        for (var i = 0; i < maxStars; i++) {
            new Star();
        }

        function animation() {
            ctx.globalCompositeOperation = 'source-over';
            ctx.globalAlpha = 0.5; //尾巴
            ctx.fillStyle = 'hsla(' + hue + ', 64%, 6%, 2)';
            ctx.fillRect(0, 0, w, h)

            ctx.globalCompositeOperation = 'lighter';
            for (var i = 1, l = stars.length; i < l; i++) {
                stars[i].draw();
            };

            window.requestAnimationFrame(animation);
        }

        animation();
    </script>
</body>
</html>
