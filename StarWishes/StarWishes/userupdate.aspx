﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="userupdate.aspx.cs" Inherits="userupdate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body style="background: url(/images/Star2.jpg)">
    <form id="form1" runat="server">
        <div style="color: white; text-align: center">
            <div style="font-size: 50px;">
                我的资料
                <br />
                <br />
            </div>
            <div>
                <asp:Label ID="Label1" runat="server" Text="用户名称:"></asp:Label>
                <asp:TextBox ID="username" runat="server"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label2" runat="server" Text="用户密码:"></asp:Label>
                <asp:TextBox ID="password" runat="server" TextMode="Password"></asp:TextBox>
                <br />
                <br />
            </div>
            <div style="width:95%">
                <asp:Label ID="Label3" runat="server" Text="用户性别:"></asp:Label>
                <asp:RadioButton ID="RadioButton1" runat="server" Text="男" GroupName="1" />
                <asp:RadioButton ID="RadioButton2" runat="server" Text="女" GroupName="1" />
                <br />
                <br />
            </div>
            <div style="width:100%">
                <asp:Label ID="Label4" runat="server" Text="用户生日:"></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>年<asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList>月
                <asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList>
                <br />
                <br />
            </div>
            <div >
                <asp:Label ID="Label5" runat="server" Text="真实姓名:"></asp:Label>
                <asp:TextBox ID="realname" runat="server"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label6" runat="server" Text="联系电话:"></asp:Label>
                <asp:TextBox ID="usertel" runat="server"></asp:TextBox>
                <br />
            </div>
            <br />
            <br />
            <div>
                <asp:Button ID="Button1" runat="server" Text="修改" OnClick="Button1_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button2" runat="server" Text="返回" OnClick="Button2_Click" />
            </div>
        </div>
        
    </form>
</body>
</html>
