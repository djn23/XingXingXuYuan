﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Register : System.Web.UI.Page
{
    StarService.StarService service = new StarService.StarService();
    protected void Page_Load(object sender, EventArgs e)
    {
        for (int i = 1950; i < 2019; i++)
        {
            DropDownList1.Items.Add(i.ToString());
        }
        if (DropDownList2.SelectedItem == null)
        {
            for (int i = 1; i <= 12; i++)
            {
                DropDownList2.Items.Add(i.ToString());
            }
        }
        for (int i = 1; i <= 31; i++)
        {
            DropDownList3.Items.Add(i.ToString());
        }

        this.tbPassword.Attributes["value"] = tbPassword.Text.Trim();
        this.tbRassword.Attributes["value"] = tbRassword.Text.Trim();
    }

    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        int year = Convert.ToInt32(DropDownList1.SelectedItem.Text);
        int month = Convert.ToInt32(DropDownList2.SelectedItem.Text);
        DropDownList3.Items.Clear();
        for (int i = 1; i <= 28; i++)
        {
            DropDownList3.Items.Add(i.ToString());
        }
        switch (month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                for (int i = 29; i <= 31; i++)
                {
                    DropDownList3.Items.Add(i.ToString());
                }
                break;
            case 2:
                if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
                {
                    DropDownList3.Items.Add("29");
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                for (int i = 29; i <= 30; i++)
                {
                    DropDownList3.Items.Add(i.ToString());
                }
                break;
        }
    }

    protected void btnReg_Click(object sender, EventArgs e)
    {
        string username = tbUserName.Text.Trim();
        string pwd = tbPassword.Text.Trim();
        string repwd = tbRassword.Text.Trim();
        int sex = 1;
        if (RadioButton2.Checked)
        {
            sex = 2;
        }
        string birthdate = DropDownList1.SelectedValue + "-" + DropDownList2.SelectedValue + "-" + DropDownList3.SelectedValue;
        string realname = tbRealName.Text.Trim();
        string telephone = tbTel.Text.Trim();

        if (pwd != repwd&&pwd!="")
        {
            Response.Write("<script>alert('两次密码不一致')</script>");
        }
        else
        {
            if (telephone.Length != 11)
            {
                Response.Write("<script>alert('电话长度应为11位')</script>");
            }
            else
            {
                //注册事件
                string result = service.Register(username,pwd,sex,birthdate,realname,telephone);
                if (Convert.ToInt32(result) > 0)
                {
                    Response.Write("<script>alert('注册成功！');location.href='Login.aspx';</script>");
                }
                else
                {
                    Response.Write("<script>alert('注册失败！请再试一次')</script>");
                }
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        //清空文本框内容
        tbUserName.Text = "";
        tbPassword.Attributes["value"] = "";
        tbRassword.Attributes["value"] = "";
        tbRealName.Text = "";
        tbTel.Text = "";
    }
}