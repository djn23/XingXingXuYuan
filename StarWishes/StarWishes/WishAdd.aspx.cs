﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WishAdd : System.Web.UI.Page
{
    StarService.StarService client = new StarService.StarService();
     string[] Team = new string[3] { "金牌团队", "银牌团队", "铜牌团队" };
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DropDownList1.DataSource = Team;
        this.DropDownList1.DataBind();
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    protected void btntj_Click(object sender, EventArgs e)
    {
        string title = Wishtitle.Text.Trim();
        string content = WishContent.Text.Trim();
        int type = 1;
        string reword = "";
        if (RadioButton2.Checked)
        {
            type = 2;
            reword = Wishreword.Text;
        }
        string usersid = Session["userid"].ToString();
        int userid = Convert.ToInt32(usersid);
        int teamid;
        string team = DropDownList1.SelectedItem.Text;
        if(team=="金牌团队")
        {
            teamid = 0;
        }
        else if(team== "银牌团队")
        {
            teamid = 1;
        }
        else
        {
            teamid = 2;
        }  
        string result=client.WishAdd(title,content,userid,type,reword,teamid);

        if(Convert.ToInt32(result)>0)
        {
            Response.Write("<script>alert('许愿成功!')</script>");
            WishContent.Text = "";
            Wishtitle.Text = "";
            RadioButton1.Checked = false;
            Wishreword.Text = "";
        }
        else
        {
            Response.Write("<script>alert('许愿失败!')</script>");
        }
       
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.location.href='WishList.aspx'</script>");
    }

   
}